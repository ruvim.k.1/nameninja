package com.gradgoose.nameninja;

import android.content.Context;

import androidx.annotation.StringRes;

import java.io.File;

public class NamesMisc {
    @StringRes
    static final int [] hiGreetings = new int [] {
            R.string.hi_im_1a,
            R.string.hi_im_2a,
            R.string.hi_im_3a,
            R.string.hi_im_1b,
            R.string.hi_im_2b,
            R.string.hi_im_3b,
            R.string.hi_im_1c,
            R.string.hi_im_2c,
            R.string.hi_im_3c,
            R.string.hi_im_1d,
            R.string.hi_im_2d,
            R.string.hi_im_3d,
    };
    static File getStatsFolder (Context context) {
        return new File (context.getFilesDir (), "stats");
    }
    static void initStatsCounts (Context context, StatHelper statHelper) {
        int justDefaultCount = People.count ("x");
        statHelper.setItemCount ("x", People.count ("x"));
        statHelper.setItemCount ("y", People.count ("y"));
        statHelper.setItemCount ("m", justDefaultCount); // Only ID 0 is used for math. However, we use a big count to weigh it down for stats purposes. 
    }
}
