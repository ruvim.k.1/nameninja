package com.gradgoose.nameninja;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

public class ViewPersonActivity extends AppCompatActivity {
    
    @Override protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_view_person);
        Intent intent = getIntent ();
        Bundle extras = intent.getExtras ();
        if (extras != null) {
            FileStatHelper statHelper = new FileStatHelper (NamesMisc.getStatsFolder (this));
            NamesMisc.initStatsCounts (this, statHelper);
            String category = extras.getString ("category");
            int id = extras.getInt ("id");
            if (category != null) {
                int metTimes = statHelper.views (category, id);
                People.Person person = People.getPerson (category, id);
                String text = "Name: " + person.name + "\nCategory: " + category + "\n\n"
                        + "You've met " + metTimes + " " + (metTimes == 1 ? "time" : "times") + ".";
                ((ImageView) findViewById (R.id.mainImage)).setImageResource (person.photo);
                ((TextView) findViewById (R.id.mainText)).setText (text);
            }
        }
    }
}
