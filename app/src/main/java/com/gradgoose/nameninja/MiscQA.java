package com.gradgoose.nameninja;

public class MiscQA {
    final String query; // An equation, term, or other "question" to ask. 
    final String result; // The answer to it. 
    MiscQA (int type, int subtype, int minVal, int maxVal) {
        switch (type) {
            case ContentFeed.ITEM_MATH_ARITH: {
                int a, b, c;
                String op;
                a = RandomInt (minVal, maxVal);
                b = RandomInt (minVal, maxVal);
                switch (subtype) {
                    case 0:
                        c = a + b;
                        op = "+";
                        break;
                    case 1:
                        c = a - b;
                        op = "-";
                        break;
                    case 2:
                        c = a * b;
                        op = "*";
                        break;
                    case 3:
                    {
                        int d = a * b; // Product. 
                        c = a; // Quotient. 
                        a = d;
                        op = "/";
                        break;
                    }
                    default:
                        a = b = c = 0;
                        op = "?";
                }
                query = a + op + b;
                result = "" + c;
                break;
            }
            default:
                query = "";
                result = "";
        }
    }
    static int RandomInt (int minVal, int maxVal) {
        return (int) (Math.random () * (maxVal - minVal) + minVal);
    }
}
