package com.gradgoose.nameninja;

import android.content.Context;

public class FeedSettings {
    final Context mContext;
    FeedSettings (Context context) {
        mContext = context;
    }
    int getInitialCount () {
        return 24;
    }
    int getPrefetchCount () {
        // Load 10 items in advance, ahead of the user's scrolling. 
        return 10;
    }
    int getSuppressCount () {
        // How many unique items must be created before an item is allowed to be repeated again in the list. 
        return 10;
    }
    double getGirlBoyRatio () {
        // 50% girls by default. 
        return 0.5;
    }
    double getQuizFrequency () {
        return 0.2;
    }
    double getGuessFrequency () {
        return 0.3;
    }
    double getMathFrequency () {
        return 0.1;
    }
    int getMathArithmeticMin () {
        return 0;
    }
    int getMathArithmeticMax () {
        return 10;
    }
    
    // TODO: Refactor the below items into a separate settings object because they are NOT feed-related. 
    int getSelectedProgressEquation () { 
        return 2;
    }
    int getSecondaryProgressEquation () {
        return 3;
    }
    int getProgressEquationCount () { 
        return ProgressEquation.EquationCount;
    }
    int getProgressUpdateInterval () {
        return 100;
    }
    int getSelectedPointsEquation () {
        return 0;
    }
    int getPointsEquationCount () {
        return PointsEquation.EquationCount;
    }
}
