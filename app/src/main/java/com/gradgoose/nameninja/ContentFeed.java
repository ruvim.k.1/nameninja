package com.gradgoose.nameninja;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.Vector;

public class ContentFeed {
    static final int ITEM_PERSON_NAME = 1; 
    static final int ITEM_PERSON_GUESS = 2; 
    static final int ITEM_PERSON_QUIZ = 3; 
    
    static final int ITEM_MATH_ARITH = 11;
    
    static boolean isQuizType (int type) {
        return type == ITEM_PERSON_GUESS || type == ITEM_PERSON_QUIZ || type == ITEM_MATH_ARITH;
    }
    static boolean isImgType (int type) {
        return type == ITEM_PERSON_NAME || type == ITEM_PERSON_GUESS || type == ITEM_PERSON_QUIZ;
    }
    static boolean isBigTextType (int type) {
        return type == ITEM_MATH_ARITH;
    }
    
    final Context mContext; 
    
    final FeedSettings settings; 
    final StatHelper stats; 
    
    ContentUpdateListener mUpdateListener = null; 
    int mLastVisible = 0; 
    
    final Vector<Item> mContent; 
    
    interface ContentUpdateListener { 
        void onContentUpdatesAvailable (); 
    }
    
    static class Item { 
        int type = 0; 
        int id = 0; 
        String bigText = "";
        String answer = "";
        String options [] = null; 
        String category = ""; 
        People.Person person = null; 
        @Override public boolean equals (Object other) { 
            if (other instanceof Item) { 
                Item otherItem = (Item) other; 
                boolean peopleEqual = false; 
                if (person == null && otherItem.person == null) 
                    peopleEqual = true; 
                if (person != null && otherItem.person != null && person.equals (otherItem.person)) 
                    peopleEqual = true; 
                return peopleEqual && type == otherItem.type && category.equals (otherItem.category) && id == otherItem.id
                        && bigText.equals (otherItem.bigText) && answer.equals (otherItem.answer);
            } 
            else return false; 
        }
    }
    
    ContentFeed (Context context, @Nullable FeedSettings feedSettings, @Nullable StatHelper statHelper) { 
        mContext = context; 
        settings = feedSettings != null ? feedSettings : new FeedSettings (context);
        stats = statHelper != null ? statHelper : new ZeroStatHelper (); 
        mContent = new Vector<> (settings.getPrefetchCount (), settings.getPrefetchCount ());
        loadMoreContent (settings.getInitialCount ()); 
    }
    
    // Call this when creating the content feed to subscribe to content updates. 
    void setContentUpdateListener (ContentUpdateListener listener) { 
        mUpdateListener = listener; 
    }
    // Call this whenever the user scrolls the list. This triggers more content on scroll down. 
    void setLastVisibleItemIndex (int lastVisibleItemIndex) { 
        mLastVisible = lastVisibleItemIndex; 
        int prefetchCount = settings.getPrefetchCount (); 
        if (mLastVisible + prefetchCount > count ()) 
            loadMoreContent (prefetchCount); 
    }
    
    // Call these to retrieve content available so far. 
    Item item (int position) { 
        synchronized (mContent) {
            return mContent.elementAt (position);
        } 
    }
    int count () { 
        synchronized (mContent) {
            return mContent.size ();
        } 
    }
    
    
    protected void loadMoreContent (int howManyMoreItems) { 
        Log.d ("Feed", "More content: " + howManyMoreItems); 
        synchronized (mContent) {
            int i = mContent.size (); 
            mContent.setSize (i + howManyMoreItems); 
            for (int j = 0; j < howManyMoreItems; j++) {
                mContent.set (i + j, loadOneItem (i + j));  
            } 
        } 
        postRunnable (mNotifyUpdatesAvailableAnyThread); 
    }
    protected Item loadOneItem (int howManyAlreadyThere) {
        Item item; 
        boolean alreadyHaveOne;
        int suppressCount = settings.getSuppressCount (); 
        int startFrom = suppressCount < howManyAlreadyThere ? howManyAlreadyThere - suppressCount : 0; 
        int stepCount = 0;
        // Purpose of the 'while' and 'for' loops: make sure we're not repeating ourself by adding the same item multiple times. 
        do { 
            item = makeRandomItem ();
            alreadyHaveOne = false; 
            for (int i = startFrom; i < howManyAlreadyThere; i++) {
                if ((item.person != null && mContent.elementAt (i).person != null 
                && item.person.name.equals (mContent.elementAt (i).person.name)) 
                || item.equals (mContent.elementAt (i))) {
                    // People names match. Need to skip. 
                    alreadyHaveOne = true;
                    break;
                }
            } 
            stepCount++;
        } while (alreadyHaveOne && stepCount < 16); 
        return item; 
    }
    protected Item makeRandomItem () { 
        double r = Math.random ();
        double freqMath = settings.getMathFrequency ();
        // TODO: Normalize frequencies to add up to 1 before using - that is, later when we have lots of them, not just math. 
        if (r < freqMath) 
            return makeMathProblem ();
        r -= freqMath;
        return makeRandomPerson ();
    }
    Item makeMathProblem () { 
        Item item = new Item ();
        item.type = ITEM_MATH_ARITH;
        item.category = "m";
        item.id = 0;
        MiscQA optionA = new MiscQA (ITEM_MATH_ARITH, (int) (Math.random () * 4), settings.getMathArithmeticMin (), settings.getMathArithmeticMax ());
        MiscQA optionB = null;
        while (optionB == null || optionA.result.equals (optionB.result)) {
            optionB = new MiscQA (ITEM_MATH_ARITH, (int) (Math.random () * 4), settings.getMathArithmeticMin (), settings.getMathArithmeticMax ());
        }
        boolean first = Math.random () < 0.5;
        item.bigText = optionA.result;
        item.answer = optionA.query;
        item.options = new String [] {
                first ? optionA.query : optionB.query, 
                first ? optionB.query : optionA.query 
        }; 
        return item;
    }
    Item makeRandomPerson () {
        Item item = new Item ();
        String xy = Math.random () >= settings.getGirlBoyRatio () ? "y" : "x";
        int peopleCount = People.count (xy);
        int randomIndex = (int) (Math.random () * peopleCount);
        item.type = ITEM_PERSON_NAME;
        item.person = People.getPerson (xy, randomIndex);
        item.bigText = item.person.name;
        item.answer = item.person.name;
        item.category = xy;
        item.id = randomIndex;
        boolean haveSeen = stats.views (xy, randomIndex) > 0;
        boolean haveQuiz = haveSeen;
        if (!haveSeen) {
            haveQuiz = Math.random () < settings.getGuessFrequency ();
        }
        if (haveQuiz) {
            boolean first = Math.random () < 0.5;
            String incorrectName = item.person.name;
            while (incorrectName.equals (item.person.name)) {
                incorrectName = People.getPerson (xy, (int) (Math.random () * peopleCount)).name;
            }
            item.type = haveSeen ? ITEM_PERSON_QUIZ : ITEM_PERSON_GUESS;
            item.options = new String [] {
                    first ? item.person.name : incorrectName,
                    first ? incorrectName : item.person.name,
            };
        }
        return item;
    }
    Handler mHandler = new Handler (); 
    void postRunnable (final Runnable runnable) { 
        mHandler.post (runnable);
    }
    Runnable mNotifyUpdatesAvailableUiThread = new Runnable () {
        @Override public void run () {
            ContentUpdateListener listener = mUpdateListener;
            Log.d ("Feed", "Notify updates");
            if (listener != null)
                listener.onContentUpdatesAvailable ();
        }
    }; 
    Runnable mNotifyUpdatesAvailableAnyThread = new Runnable () {
        @Override public void run () {
            if (mContext instanceof Activity)
                ((Activity) mContext).runOnUiThread (mNotifyUpdatesAvailableUiThread);
        }
    }; 
}
