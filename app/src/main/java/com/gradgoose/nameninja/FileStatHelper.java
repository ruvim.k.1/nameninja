package com.gradgoose.nameninja;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

public class FileStatHelper extends StatHelper {
    static final String TAG = "StatHelper"; 
    
    final File mFolder;
    final HashMap<String, int []> mViews;
    final HashMap<String, float []> mDur;
    final HashMap<String, float []> mDurLast;
    final HashMap<String, double []> mLast;
    final HashMap<String, float []> mPts;
    
    FileStatHelper (File folder) { 
        mFolder = folder;
        if (!mFolder.exists () && !mFolder.mkdirs ()) {
            Log.e (TAG, "Could not create folder " + mFolder.getAbsolutePath ());
        }
        Log.i (TAG, "Stats folder: " + mFolder.getAbsolutePath ());
        mViews = new HashMap<> ();
        mDur = new HashMap<> ();
        mDurLast = new HashMap<> ();
        mLast = new HashMap<> ();
        mPts = new HashMap<> ();
    }
    
    void loadData (String category) { 
        int itemCount = getItemCount (category);
        int [] arrViews = mViews.get (category);
        float [] arrDur = mDur.get (category);
        float [] arrDurLast = mDurLast.get (category);
        double [] arrLast = mLast.get (category);
        float [] arrPts = mPts.get (category);
        if (arrViews == null) 
            mViews.put (category, arrViews = new int [itemCount]);
        if (arrDur == null) 
            mDur.put (category, arrDur = new float [itemCount]);
        if (arrDurLast == null)
            mDurLast.put (category, arrDurLast = new float [itemCount]);
        if (arrLast == null)
            mLast.put (category, arrLast = new double [itemCount]);
        if (arrPts == null)
            mPts.put (category, arrPts = new float [itemCount]);
        try {
            FileInputStream stream;
            BasicTypeArrayReader.Read (stream = new FileInputStream (new File (mFolder, category + "-views.int")), arrViews);
            stream.close ();
            BasicTypeArrayReader.Read (stream = new FileInputStream (new File (mFolder, category + "-dur.f32")), arrDur);
            stream.close ();
            BasicTypeArrayReader.Read (stream = new FileInputStream (new File (mFolder, category + "-dur-last.f32")), arrDurLast);
            stream.close ();
            BasicTypeArrayReader.Read (stream = new FileInputStream (new File (mFolder, category + "-last.f64")), arrLast);
            stream.close ();
            BasicTypeArrayReader.Read (stream = new FileInputStream (new File (mFolder, category + "-pts.f32")), arrPts);
            stream.close ();
        } 
        catch (IOException err) {
            Log.e (TAG, "Could not load data for category " + category + "; " + err.getLocalizedMessage ());
        }
    }
    void saveViews (String category) {
        Log.d (TAG, "Command received to save views for category " + category);
        int [] arrViews = mViews.get (category);
        if (arrViews == null) return;
        try {
            FileOutputStream stream;
            BasicTypeArrayWriter.Write (stream = new FileOutputStream (new File (mFolder, category + "-views.int")), arrViews);
            stream.close ();
        }
        catch (IOException err) {
            Log.e (TAG, "Could not save data for category " + category + " (views); " + err.getLocalizedMessage ());
        }
    }
    void saveDur (String category) {
        Log.d (TAG, "Command received to save dur for category " + category);
        float [] arrDur = mDur.get (category);
        if (arrDur == null) return;
        try {
            FileOutputStream stream;
            BasicTypeArrayWriter.Write (stream = new FileOutputStream (new File (mFolder, category + "-dur.f32")), arrDur);
            stream.close ();
        }
        catch (IOException err) {
            Log.e (TAG, "Could not save data for category " + category + " (dur); " + err.getLocalizedMessage ());
        }
    }
    void saveDurLast (String category) {
        Log.d (TAG, "Command received to save dur for category " + category);
        float [] arrDurLast = mDurLast.get (category);
        if (arrDurLast == null) return;
        try {
            FileOutputStream stream;
            BasicTypeArrayWriter.Write (stream = new FileOutputStream (new File (mFolder, category + "-dur-last.f32")), arrDurLast);
            stream.close ();
        }
        catch (IOException err) {
            Log.e (TAG, "Could not save data for category " + category + " (dur last); " + err.getLocalizedMessage ());
        }
    }
    void saveLast (String category) {
        Log.d (TAG, "Command received to save last for category " + category);
        double [] arrLast = mLast.get (category);
        if (arrLast == null) return;
        try {
            FileOutputStream stream;
            BasicTypeArrayWriter.Write (stream = new FileOutputStream (new File (mFolder, category + "-last.f64")), arrLast);
            stream.close ();
        }
        catch (IOException err) {
            Log.e (TAG, "Could not save data for category " + category + " (last); " + err.getLocalizedMessage ());
        }
    }
    void savePts (String category) {
        Log.d (TAG, "Command received to save ans pts for category " + category);
        float [] arrPts = mPts.get (category);
        if (arrPts == null) return;
        try {
            FileOutputStream stream;
            BasicTypeArrayWriter.Write (stream = new FileOutputStream (new File (mFolder, category + "-pts.f32")), arrPts);
            stream.close ();
        }
        catch (IOException err) {
            Log.e (TAG, "Could not save data for category " + category + " (pts); " + err.getLocalizedMessage ());
        }
    }
    
    @Override StatHelper setItemCount (String category, int count) { 
        super.setItemCount (category, count);
        loadData (category);
        return this;
    }
    
    @Override int views (String category, int position) {
        int [] arrViews = mViews.containsKey (category) ? mViews.get (category) : null;
        return arrViews != null && position < arrViews.length ? arrViews [position] : 0;
    }
    
    @Override void view (String category, int position, int addCount) {
        int [] arrViews = mViews.containsKey (category) ? mViews.get (category) : null;
        if (arrViews != null && position < arrViews.length) {
            Log.d (TAG, "Found views item for " + category + " position " + position + ": " + arrViews [position]);
            arrViews [position] += addCount;
            saveViews (category);
        }
        Log.d (TAG, "Item " + category + ":" + position + " views += " + addCount); 
    }
    
    @Override float viewDuration (String category, int position) {
        float [] arrDur = mDur.containsKey (category) ? mDur.get (category) : null;
        return arrDur != null && position < arrDur.length ? arrDur [position] : 0;
    }
    
    @Override void viewFor (String category, int position, float seconds) {
        Log.d (TAG, "Item " + category + ":" + position + " duration += " + seconds);
        float [] arrDur = mDur.containsKey (category) ? mDur.get (category) : null;
        if (arrDur != null && position < arrDur.length) {
            Log.d (TAG, "Found dur item for " + category + " position " + position + ": " + arrDur [position]);
            arrDur [position] += seconds;
            saveDur (category);
        }
        float [] arrDurLast = mDurLast.containsKey (category) ? mDurLast.get (category) : null;
        if (arrDurLast != null && position < arrDurLast.length) {
            arrDurLast [position] = seconds;
            saveDurLast (category);
        }
    }
    
    @Override float lastDuration (String category, int position) {
        float [] arrDurLast = mDurLast.containsKey (category) ? mDurLast.get (category) : null;
        return arrDurLast != null && position < arrDurLast.length ? arrDurLast [position] : 0;
    }
    
    @Override double lastViewed (String category, int position) {
        double [] arrLast = mLast.containsKey (category) ? mLast.get (category) : null;
        return arrLast != null && position < arrLast.length ? arrLast [position] : 0;
    }
    
    @Override void viewAt (String category, int position, double timestamp) {
        Log.d (TAG, "Item " + category + ":" + position + " lastViewed = " + timestamp);
        double [] arrLast = mLast.containsKey (category) ? mLast.get (category) : null;
        if (arrLast != null && position < arrLast.length) {
            Log.d (TAG, "Found last item for " + category + " position " + position + ": " + arrLast [position]);
            arrLast [position] = timestamp;
            saveLast (category);
        }
    }
    
    @Override float getAnswerPoints (String category, int position) {
        float [] arrPts = mPts.containsKey (category) ? mPts.get (category) : null;
        return arrPts != null && position < arrPts.length ? arrPts [position] : 0;
    }
    
    @Override void setAnswerPoints (String category, int position, float points) {
        float [] arrPts = mPts.containsKey (category) ? mPts.get (category) : null;
        if (arrPts != null && position < arrPts.length) {
            Log.d (TAG, "Found pts item for " + category + " position " + position + ": " + arrPts [position]);
            arrPts [position] = points;
            savePts (category);
        }
    }
}
