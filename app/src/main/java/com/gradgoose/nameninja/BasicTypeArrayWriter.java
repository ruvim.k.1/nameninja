package com.gradgoose.nameninja;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class BasicTypeArrayWriter {
    static void Write (OutputStream outputStream, int [] buf) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate (buf.length * 4);
        buffer.asIntBuffer ().put (buf);
        outputStream.write (buffer.array ());
    }
    static void Write (OutputStream outputStream, float [] buf) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate (buf.length * 4);
        buffer.asFloatBuffer ().put (buf);
        outputStream.write (buffer.array ());
    }
    static void Write (OutputStream outputStream, double [] buf) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate (buf.length * 8);
        buffer.asDoubleBuffer ().put (buf);
        outputStream.write (buffer.array ());
    }
}
