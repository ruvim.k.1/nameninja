package com.gradgoose.nameninja;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;

public class MainProgressManager {
    static final String TAG = "ProgressMgr";
    
    final Context mContext;
    final ProgressBar mBar;
    final int mTotal;
    
    boolean mChanged = false;
    int mProgress = 0;
    int mSecondaryProgress = 0;
    
    FeedSettings mSettings = null;
    StatHelper mStats = null;
    
    Handler mHandler = new Handler ();
    
    MainProgressManager (Context context, ProgressBar bar) {
        mContext = context;
        mBar = bar;
        mTotal = bar.getMax ();
        init ();
    }
    
    void init () { 
        mBar.setProgress (0);
        mBar.setSecondaryProgress (0);
        mHandler.postDelayed (mStep, 100); // Begin delay loop.
    }
    
    void computeProgress () { 
        FeedSettings settings = mSettings;
        if (settings == null) {
            Log.e (TAG, "Settings is null; cannot compute primary progress");
            return;
        }
        float progress = ProgressEquation.ComputeProgress (settings.getSelectedProgressEquation (), "", mStats);
        int asInteger = (int) (progress * mTotal);
        mChanged |= asInteger != mProgress;
        mProgress = asInteger;
    }
    void computeSecondaryProgress () {
        FeedSettings settings = mSettings;
        if (settings == null) {
            Log.e (TAG, "Settings is null; cannot compute secondary progress");
            return;
        }
        float progress = ProgressEquation.ComputeProgress (settings.getSecondaryProgressEquation (), "", mStats);
        int asInteger = (int) (progress * mTotal);
        mChanged |= asInteger != mSecondaryProgress;
        mSecondaryProgress = asInteger;
    }
    
    private Runnable mStep = new Runnable () {
        @Override public void run () {
            computeProgress ();
            computeSecondaryProgress ();
            if (mChanged) {
                if (mContext instanceof Activity) {
                    ((Activity) mContext).runOnUiThread (mUpdate);
                }
            }
            FeedSettings settings = mSettings;
            int delay = settings != null ? settings.getProgressUpdateInterval () : 100;
            mHandler.postDelayed (mStep, delay);
        }
    };
    private Runnable mUpdate = new Runnable () {
        @Override public void run () {
            mBar.setProgress (mProgress);
            mBar.setSecondaryProgress (mSecondaryProgress);
        }
    };
    
    MainProgressManager setFeedSettings (FeedSettings settings) {
        mSettings = settings;
        return this;
    }
    MainProgressManager setStatHelper (StatHelper helper) {
        mStats = helper;
        return this;
    }
}
