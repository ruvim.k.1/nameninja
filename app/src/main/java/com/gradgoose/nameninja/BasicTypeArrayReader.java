package com.gradgoose.nameninja;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class BasicTypeArrayReader {
    static int Read (InputStream inputStream, int [] outbuf) throws IOException {
        int avl = inputStream.available ();
        ByteBuffer buffer = ByteBuffer.allocate (avl); 
        int bRead = inputStream.read (buffer.array ());
        buffer.asIntBuffer ().get (outbuf);
        return bRead / 4;
    }
    static int Read (InputStream inputStream, float [] outbuf) throws IOException { 
        int avl = inputStream.available ();
        ByteBuffer buffer = ByteBuffer.allocate (avl);
        int bRead = inputStream.read (buffer.array ());
        buffer.asFloatBuffer ().get (outbuf);
        return bRead / 4;
    }
    static int Read (InputStream inputStream, double [] outbuf) throws IOException {
        int avl = inputStream.available ();
        ByteBuffer buffer = ByteBuffer.allocate (avl);
        int bRead = inputStream.read (buffer.array ());
        buffer.asDoubleBuffer ().get (outbuf);
        return bRead / 8;
    }
}
