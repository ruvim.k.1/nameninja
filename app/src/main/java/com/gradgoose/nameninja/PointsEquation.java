package com.gradgoose.nameninja;

public class PointsEquation {
    static final int EquationCount = 1;
    static float PointsIfCorrectFirstTry (int equationID, float wasPoints) {
        return 0.5f;
    }
    static float PointsIfCorrectAfterFixing (int equationID, float wasPoints) {
        return 0.75f;
    }
    static float PointsIfIncorrect (int equationID, float wasPoints) {
        return wasPoints;
    }
}
