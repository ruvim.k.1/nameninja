package com.gradgoose.nameninja;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;

public class MainRvManager {
    final Context mContext; 
    final RecyclerView mRV; 
    final MainListAdapter adapter; 
    final LinearLayoutManager layoutManager; 
    final ContentFeed contentFeed; 
    final FeedSettings feedSettings;
    StatHelper statHelper; 
    public MainRvManager (Context context, RecyclerView rv) { 
        mContext = context; 
        mRV = rv;
        feedSettings = new FeedSettings (context);
        adapter = new MainListAdapter (context);
        layoutManager = new EndlessScrollLayoutManager (context, RecyclerView.VERTICAL, false).setFeedSettings (feedSettings); 
        statHelper = new FileStatHelper (NamesMisc.getStatsFolder (mContext)); 
        NamesMisc.initStatsCounts (mContext, statHelper);
        contentFeed = new ContentFeed (context, feedSettings, statHelper); 
        init (); 
    }
    void init () { 
        contentFeed.setContentUpdateListener (adapter); 
        adapter.setStatHelper (statHelper); 
        adapter.setContentFeed (contentFeed); 
        mRV.setLayoutManager (layoutManager); 
        mRV.setAdapter (adapter); 
        mRV.addOnScrollListener (new RecyclerView.OnScrollListener () {
            @Override
            public void onScrollStateChanged (@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged (recyclerView, newState);
            }
    
            @Override
            public void onScrolled (@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled (recyclerView, dx, dy);
                int lastVisible = layoutManager.findLastVisibleItemPosition ();
                contentFeed.setLastVisibleItemIndex (lastVisible);
            }
        });
    }
    FeedSettings getFeedSettings () { 
        return feedSettings;
    }
    StatHelper getStatHelper () { 
        return statHelper;
    }
}
