package com.gradgoose.nameninja;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class EndlessScrollLayoutManager extends LinearLayoutManager {
    FeedSettings mSettings = null;
    public EndlessScrollLayoutManager (Context context, int orientation, boolean reverseLayout) {
        super (context, orientation, reverseLayout);
    }
    EndlessScrollLayoutManager setFeedSettings (FeedSettings settings) { 
        mSettings = settings;
        return this;
    }
    
    @Override
    public int computeVerticalScrollOffset (RecyclerView.State state) {
        FeedSettings settings = mSettings;
        if (settings == null)
            return super.computeVerticalScrollOffset (state);
        int firstVisible = findFirstVisibleItemPosition ();
        int lastVisible = findLastVisibleItemPosition ();
        View firstView = findViewByPosition (firstVisible);
        if (firstView == null) 
            return super.computeVerticalScrollOffset (state);
        int top = -getDecoratedTop (firstView);
        int height = getDecoratedMeasuredHeight (firstView);
        int prefetch = settings.getPrefetchCount ();
        int scale = computeVerticalScrollRange (state);
        if (height <= 0) 
            height = 1;
        return (firstVisible * scale + top * scale / height) / (lastVisible + prefetch); 
    }
    
    @Override
    public int computeVerticalScrollExtent (RecyclerView.State state) {
        return super.computeVerticalScrollExtent (state);
    }
    
    @Override
    public int computeVerticalScrollRange (RecyclerView.State state) {
        return super.computeVerticalScrollRange (state);
    }
}
