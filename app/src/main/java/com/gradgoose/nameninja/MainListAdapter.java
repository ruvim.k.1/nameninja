package com.gradgoose.nameninja;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Vector;

public class MainListAdapter extends RecyclerView.Adapter implements ContentFeed.ContentUpdateListener {
    static final String TAG = "MainList";
    
    final Context mContext; 
    
    final Object mMutex = new Object (); 
    
    StatHelper stats = null; 
    ContentFeed mFeed = null; 
    
    RecyclerView rvAttached = null; 
    
    // Keeps track of greeting strings for everyone. 
    Vector<String> mGreetings = new Vector<> (); 
    SparseArray<String> mUserAnswers = new SparseArray<> (); 
    
    public MainListAdapter (Context context) { 
        mContext = context; 
    }
    
    void setStatHelper (StatHelper statHelper) { 
        stats = statHelper;
    }
    void setContentFeed (ContentFeed feed) { 
        synchronized (mMutex) {
            mFeed = feed;
        } 
        growLists (); 
        notifyDataSetChanged (); 
    }
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder (@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ContentFeed.ITEM_PERSON_NAME: 
            case ContentFeed.ITEM_PERSON_GUESS:
            case ContentFeed.ITEM_PERSON_QUIZ:
            case ContentFeed.ITEM_MATH_ARITH:
                return new ItemNameHolder (LayoutInflater.from (mContext).inflate (R.layout.item_name, parent, false));  
        } 
        return new PlaceholderViewHolder (new View (mContext)); 
    }
    
    @Override
    public void onBindViewHolder (@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemNameHolder) {
            synchronized (mMutex) {
                ((ItemNameHolder) holder).bind (mFeed.item (position), position);
            } 
        } 
    }
    
    @Override
    public int getItemViewType (int position) {
        synchronized (mMutex) {
            return mFeed.item (position).type; 
        } 
    }
    
    @Override
    public long getItemId (int position) {
        return super.getItemId (position);
    }
    
    @Override
    public int getItemCount () {
        synchronized (mMutex) {
            return mFeed.count (); 
        } 
    }
    
    @Override
    public void onAttachedToRecyclerView (@NonNull RecyclerView recyclerView) {
        rvAttached = recyclerView; 
        super.onAttachedToRecyclerView (recyclerView);
    }
    
    @Override
    public void onDetachedFromRecyclerView (@NonNull RecyclerView recyclerView) {
        rvAttached = null; 
        super.onDetachedFromRecyclerView (recyclerView);
    }
    
    @Override
    public void onContentUpdatesAvailable () { 
        growLists (); 
        notifyDataSetChanged (); 
    }
    
    void growLists () {
        ContentFeed feed = mFeed;
        if (feed != null) {
            int was = mGreetings.size ();
            mGreetings.setSize (feed.count ());
            for (int i = was; i < mGreetings.size (); i++) {
                int randomIndex = (int) (Math.random () * NamesMisc.hiGreetings.length);
                String greeting = mContext.getString (NamesMisc.hiGreetings [randomIndex]); 
                if (i > 0 && mGreetings.elementAt (i - 1).equals (greeting)) {
                    // We're about to use two of the same greeting in a row. Let's not do that, 
                    // so that we don't bore the user out. 
                    i--; 
                    continue; // This will cause a redo of the current 'for' iteration. 
                } 
                mGreetings.set (i, greeting);
            }
        }
    }
    
    @Override
    public void onViewAttachedToWindow (@NonNull RecyclerView.ViewHolder holder) {
        if (holder instanceof ItemNameHolder) {
            ((ItemNameHolder) holder).onViewStart ();
        }
    }
    
    @Override
    public void onViewDetachedFromWindow (@NonNull RecyclerView.ViewHolder holder) {
        if (holder instanceof ItemNameHolder) {
            ((ItemNameHolder) holder).onViewStop ();
        }
    }
    
    class ItemNameHolder extends RecyclerView.ViewHolder {
        final ImageView img; 
        final TextView bigText;
        final TextView text; 
        final TextView option1; 
        final TextView option2; 
        ContentFeed.Item item = null; 
        int iPosition = 0; 
        double mViewStartTime = 0;
        boolean selectAnswerOption (String answerOption) {
            int optionSelected = item.options [0].equals (answerOption) ? 0 : 1;
            boolean correct = answerOption.equals (item.answer);
            option1.setText (item.options [0]);
            option2.setText (item.options [1]);
            if (answerOption.isEmpty ()) {
                int strRes; 
                switch (item.type) {
                    case ContentFeed.ITEM_PERSON_GUESS:
                        strRes = R.string.guess_name;
                        break;
                    case ContentFeed.ITEM_MATH_ARITH:
                        strRes = R.string.math_arith_question;
                        break;
                    default:
                        strRes = R.string.remember_name;
                } 
                text.setText (mContext.getString (strRes).replace ("NAME", item.bigText));
                option1.setTextColor (mContext.getResources ().getColor (R.color.optionDefault));
                option2.setTextColor (mContext.getResources ().getColor (R.color.optionDefault));
                option1.setVisibility (View.VISIBLE);
                option2.setVisibility (View.VISIBLE);
                return false; 
            } 
            if (correct) {
                if (optionSelected == 0) {
                    option1.setTextColor (mContext.getResources ().getColor (R.color.colorCorrect));
                    option2.setVisibility (View.INVISIBLE);
                } else {
                    option1.setVisibility (View.INVISIBLE);
                    option2.setTextColor (mContext.getResources ().getColor (R.color.colorCorrect));
                }
                int strRes;
                switch (item.type) {
                    case ContentFeed.ITEM_MATH_ARITH:
                        strRes = R.string.comment_correct_math;
                        break;
                    default:
                        strRes = R.string.comment_correct_01;
                }
                text.setText (mContext.getString (strRes).replace ("NAME", item.bigText));
            }
            else {
                if (optionSelected == 0) {
                    text.setText (mContext.getString (R.string.comment_incorrect_01).replace ("TEXT", item.options [0]));
                    option1.setText (mContext.getString (R.string.option_wrong).replace ("TEXT", item.options [0]));
                    option1.setTextColor (mContext.getResources ().getColor (R.color.colorWrong));
                } else {
                    text.setText (mContext.getString (R.string.comment_incorrect_01).replace ("TEXT", item.options [1]));
                    option2.setText (mContext.getString (R.string.option_wrong).replace ("TEXT", item.options [1]));
                    option2.setTextColor (mContext.getResources ().getColor (R.color.colorWrong));
                }
            }
            return correct;
        }
        public ItemNameHolder (View root) {
            super (root);
            img = root.findViewById (R.id.itemImage); 
            bigText = root.findViewById (R.id.itemBigText);
            text = root.findViewById (R.id.itemText); 
            option1 = root.findViewById (R.id.itemOption1); 
            option2 = root.findViewById (R.id.itemOption2);
            View.OnClickListener listener = new View.OnClickListener () {
                @Override public void onClick (View view) {
                    ContentFeed.Item item = ItemNameHolder.this.item; 
                    if (item == null) return; 
                    int optionSelected = view == option1 ? 0 : 1; 
                    String userAnswer = item.options [optionSelected];
                    boolean correct = selectAnswerOption (userAnswer); 
                    float points = stats.getAnswerPoints (item.category, item.id);
                    String ansBefore = mUserAnswers.get (iPosition, "");
                    int equationID = 0;
                    FeedSettings settings = mFeed.settings;
                    if (settings != null) 
                        equationID = settings.getSelectedPointsEquation ();
                    mUserAnswers.put (iPosition, userAnswer); 
                    Log.i (TAG, "Answer before: " + ansBefore + "; answer now: " + userAnswer);
                    if (correct) {
                        if (!userAnswer.equals (ansBefore) && !ansBefore.isEmpty ()) {
                            // Previous answer was not correct, but this one is.
                            points = PointsEquation.PointsIfCorrectAfterFixing (equationID, points);
                            Log.i (TAG, "Correct second attempt");
                        }
                        else {
                            // Previous no answer, so this might be correct on the first try.
                            points = PointsEquation.PointsIfCorrectFirstTry (equationID, points);
                            Log.i (TAG, "Correct first try");
                        }
                        updateLastViewTimestamp ();
                        updateViewDuration ();
                    }
                    else if (ansBefore.isEmpty ()) {
                        // Incorrect answer. 
                        points = PointsEquation.PointsIfIncorrect (equationID, points);
                        Log.i (TAG, "Incorrect");
                    }
                    stats.setAnswerPoints (item.category, item.id, points);
                }
            }; 
            option1.setOnClickListener (listener);
            option2.setOnClickListener (listener);
            img.setOnClickListener (new View.OnClickListener () {
                @Override public void onClick (View view) {
                    Intent intent = new Intent (mContext, ViewPersonActivity.class);
                    intent.putExtra ("category", item.category);
                    intent.putExtra ("id", item.id);
                    mContext.startActivity (intent);
                }
            });
        }
        void bind (ContentFeed.Item item, int position) {
            People.Person person = item.person; 
            this.item = item;
            iPosition = position;
            bigText.setVisibility (ContentFeed.isBigTextType (item.type) ? View.VISIBLE : View.GONE);
            bigText.setText (item.bigText);
            if (ContentFeed.isQuizType (item.type)) {
                selectAnswerOption (mUserAnswers.get (position, "")); // Put default answer options.
            } 
            else {
                if (item.type == ContentFeed.ITEM_PERSON_NAME)
                    text.setText (mGreetings.elementAt (position).replace ("NAME", person.name));
                // Hide options: 
                option1.setVisibility (View.GONE);
                option2.setVisibility (View.GONE);
            }
            if (person != null && person.photo != 0) 
                img.setImageResource (person.photo); 
            img.setVisibility (ContentFeed.isImgType (item.type) ? View.VISIBLE : View.GONE);
        }
        void onViewStart () {
            StatHelper helper = stats;
            mViewStartTime = StatHelper.TimeNow ();
            if (helper != null) {
                helper.view (item.category, item.id, 1);
            }
        }
        void onViewStop () {
            updateLastViewTimestamp ();
            updateViewDuration ();
        }
        void updateLastViewTimestamp () {
            StatHelper helper = stats;
            double now = StatHelper.TimeNow ();
            if (helper != null) {
                helper.viewAt (item.category, item.id, now);
            }
        }
        void updateViewDuration () {
            StatHelper helper = stats;
            double now = StatHelper.TimeNow ();
            if (helper != null) {
                Log.i (TAG, "Viewed for " + (now - mViewStartTime) + " seconds");
                helper.viewFor (item.category, item.id, (float) (now - mViewStartTime));
            }
            mViewStartTime = now;
        }
    }
    class PlaceholderViewHolder extends RecyclerView.ViewHolder { 
        public PlaceholderViewHolder (View root) { 
            super (root); 
        }
        void bind (Object dataItem, int position) { 
            
        }
    }
}
