package com.gradgoose.nameninja;

import android.util.Log;

import java.util.Set;

public class ProgressEquation {
    static final String TAG = "ProgressEquation";
    static final int EquationCount = 4;
    // Note: Progress will be a fraction >= 0.0 and <= 1.0; 
    static float ComputeProgress (int equationID, String category, StatHelper statHelper) { 
        if (equationID < EquationCount) {
            double now = StatHelper.TimeNow ();
            float sum = EquationBegin (equationID);
            int catCount = 0;
            if (category == null || category.isEmpty ()) {
                for (String cat : statHelper.getCategories ()) {
                    sum += EquationTerm (equationID, now, cat, statHelper);
                    catCount++;
                }
            }
            else { 
                sum += EquationTerm (equationID, now, category, statHelper);
                catCount = 1;
            }
            return EquationEnd (equationID, sum, catCount);
        }
        return equationID / 10.0f + 0.2f; // Just a test.
    }
    static float EquationBegin (int equationID) {
        return 0.0f;
    }
    static float EquationTerm (int equationID, double timeNow, String category, StatHelper statHelper) { 
        int count = statHelper.getItemCount (category);
        float sum = 0;
        for (int i = 0; i < count; i++) {
            sum += EquationTerm (equationID, timeNow, category, i, count, statHelper);
        }
        return sum;
    }
    static float EquationTerm (int equationID, double timeNow, String category, int idWithinCategory, int countWithinCategory, StatHelper statHelper) {
        float elapsed = (float) (timeNow - statHelper.lastViewed (category, idWithinCategory));
        int views = statHelper.views (category, idWithinCategory);
        float dur = statHelper.viewDuration (category, idWithinCategory);
        float dl = statHelper.lastDuration (category, idWithinCategory);
        float pts = statHelper.getAnswerPoints (category, idWithinCategory);
        double dlFactor = Math.max (Math.min (dl - 2, 1), 0); // Ensures user looks at every item for at least 2 seconds. 
        double result;
        switch (equationID) {
            case 0:
                result = Math.exp (-elapsed / (60)) / countWithinCategory;
                break;
            case 1:
                result = views > 0 ? 1.0f / countWithinCategory : 0;
                break;
            case 2:
                result = Math.exp (-elapsed / (360)) / countWithinCategory * dlFactor; //(1- Math.exp (- dl * dl / (1)));
                result += pts * Math.exp (-elapsed / (60)) * (1.0 - result) * 0.1 * dlFactor;
                if (result > 1e-1) Log.d (TAG, "Result: " + result);
                break;
            case 3:
                result = Math.exp (-elapsed / (60 * 60)) / countWithinCategory;
                break;
            default:
                result = 0;
        }
        return (float) result;
    }
    static float EquationEnd (int equationID, float sum, int categoryCount) {
        switch (equationID) {
            case 0:
            case 1:
            case 2:
            case 3:
                return sum / categoryCount;
            default:
                return 0;
        }
    }
}
