package com.gradgoose.nameninja;

public class ZeroStatHelper extends StatHelper {
    @Override
    int views (String category, int position) {
        return 0;
    }
    
    @Override
    void view (String category, int position, int addCount) {
        
    }
    
    @Override
    float viewDuration (String category, int position) {
        return 0;
    }
    
    @Override
    void viewFor (String category, int position, float seconds) {
        
    }
    
    @Override
    float lastDuration (String category, int position) {
        return 0;
    }
    
    @Override
    double lastViewed (String category, int position) {
        return 0;
    }
    
    @Override
    void viewAt (String category, int position, double timestamp) {
        
    }
    
    @Override
    float getAnswerPoints (String category, int position) {
        return 0;
    }
    
    @Override
    void setAnswerPoints (String category, int position, float points) {
        
    }
}
