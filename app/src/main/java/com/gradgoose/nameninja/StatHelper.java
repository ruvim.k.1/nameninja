package com.gradgoose.nameninja;

import androidx.annotation.Nullable;

import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

abstract class StatHelper {
    final HashMap<String, Integer> mItemCounts; 
    StatHelper () { 
        mItemCounts = new HashMap<> (); 
    }
    abstract int views (String category, int position); // Determine view count. 
    abstract void view (String category, int position, int addCount); // Add to view count ("view" this item). 
    abstract float viewDuration (String category, int position); // How many seconds this was viewed for total. 
    abstract void viewFor (String category, int position, float seconds); // Add to view duration. 
    abstract float lastDuration (String category, int position); // How many seconds this was viewed for last time it was viewed. 
    abstract double lastViewed (String category, int position); // Timestamp of last viewed. 
    abstract void viewAt (String category, int position, double timestamp); // Set last viewed time. 
    abstract float getAnswerPoints (String category, int position); // Correct/incorrect score for this person.
    abstract void setAnswerPoints (String category, int position, float points);
    StatHelper setItemCount (String category, int count) { 
        mItemCounts.put (category, count); 
        return this; 
    }
    int getItemCount (String category) {
        return mItemCounts.containsKey (category) ? mItemCounts.get (category) : 0;
    }
    Set<String> getCategories () { 
        return mItemCounts.keySet ();
    }
    int countConditional (@Nullable String opt_category, int minViews, int maxViews, float minDur, float maxDur,
                          double minLastTime, double maxLastTime) { 
        if (opt_category == null || opt_category.isEmpty ()) {
            int total = 0; 
            for (String cat : mItemCounts.keySet ()) {
                if (cat.isEmpty ()) continue; // Just in case there's a category like that... Phew! 
                total += countConditional (cat, minViews, maxViews, minDur, maxDur, minLastTime, maxLastTime); 
            } 
            return total; 
        }
        HashMap<String, Integer> mapCount = mItemCounts; 
        int count = 0; 
        int items = getItemCount (opt_category);
        for (int i = 0; i < items; i++) {
            int statViews = views (opt_category, i); 
            float statDur = viewDuration (opt_category, i); 
            double statLast = lastViewed (opt_category, i); 
            if (statViews < minViews || statDur < minDur || statLast < minLastTime) {
                continue;
            } 
            if (maxViews != -1 && statViews > maxViews) continue; 
            if (maxDur != -1 && statDur > maxDur) continue;
            if (maxLastTime != -1 && statLast > maxLastTime) continue;
            count++; 
        } 
        return count; 
    }
    static double TimeNow () { 
        return new Date ().getTime () / 1e3; // Returns seconds as UNIX time since epoch. 
    }
}
